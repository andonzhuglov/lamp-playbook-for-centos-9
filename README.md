# LAMP for CentOS 9

The playbook itself is my very first one and was elaborated to install services from the LAMP Stack (Linux, Apache, MySQL and PHP) using the latest versions. In addition it will set a password for the MySQL root user, remove the test database, configure the Virtual Host and pull a custom home page.

## Structure

```shell
.
├── files
│   └── apache.conf.j2
├── playbook.yml
├── README.md
└── vars
    └── default.yml

2 directories, 4 files
```
What are the files used for?

- `files/apache.conf.j2` This is our Template file that we are using for the Virtual Host.
- `playbook.yml` Our Playbook file that contains the aforementioned tasks.
- `README.md` The Markdown file stores information about the repo.
- `vars/default.yml` is used to set up the variables for our Playbook.

## Setting up the vars/default.yml

- `mysql_root_pass`: the password for the MySQL root account.
- `app_user`: a remote non-root user on the Ansible host that will own the application files.
- `http_host`: our domain name.
- `http_conf`: the name of the configuration file that will be created within Apache.
- `http_port`: We have set our HTTP Port to 80 by default.

## Environment set up and connection between Master and Slaves

The Playbook is configured to deploy MySQL to our (dbservers) and Apache + PHP in our (webservers). For both the Master and Slave servers make sure to have installed the CentOS EPEL repository with `sudo yum install epel-release`. This will allow us to easily  install Ansible with `sudo yum install ansible` -  updating packages is recommended with `sudo yum update`. 

The default Ansible inventory file is located in `/etc/ansible/hosts` and used to obtain the target hosts for the installation:

```
[webservers]
12.12.12.12

[dbservers]
14.14.14.14
```

![Diagram](https://snipboard.io/b6UYcD.jpg)

SSH authetication is used to connect the Ansible Master Server to Slave 1 and Slave 2 Ansible Servers. For the connection we generate SSH Key and display the public key (note that the default name for the public key  is `id_rsa.pub`:

```command
ssh-keygen
cat ~/.ssh/keyname.pub
```

Next we log to our Slave 1 and Slave 2 Servers and append the public key to the end of the authorized_keys file with:

```command
cat >> ~/.ssh/authorized_keys
```

Once this is completed

## Running our Playbook

### 1. Clone the repo to your server where Ansible is installed
```shell
git clone https://gitlab.com/andonzhuglov/lamp-playbook-for-centos-9.git
```

### 2. Customizable Settings in vars/default.yml

Content:
```yml
---
mysql_root_pass: "mysql_root_password"
app_user: "root"
http_host: "domain.com"
http_conf: "domain.com.conf"
http_port: "80"
```

### 3. Running the Playbook

```command
cd lamp-playbook-for-centos-9
ansible-playbook playbook.yml
```

The Playbook was tested using CentOS 9 servers in DigiOcean.com. Future plans for the Playbook is to tailor it further while learning more about Ansible.
